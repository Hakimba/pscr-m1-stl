/*
 * List.h
 *
 *  Created on: 4 oct. 2019
 *      Author: 3602783
 */

#ifndef LIST_H_
#define LIST_H_

namespace pr {
template <typename T>

class List {
	class Chainon {
		T data;
		Chainon* next;
	private:
		Chainon(T data);
		Chainon(const Chainon&);
		Chainon& operator = (Chainon& s);
		~Chainon();
	};

	Chainon* tete;

private:
	List();
	List(Chainon* t);
	List(const List&);
	List& operator = (List& s);
	~List();

};
}


#endif /* LIST_H_ */
