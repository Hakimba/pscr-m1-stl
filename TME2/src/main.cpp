//============================================================================
// Name        : TME2.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <fstream>
#include <regex>
#include <chrono>


bool isInVec(std::vector<std::pair<std::string,int>>& vec, std::string word){
	for (auto el : vec) {
		if(el.first == word)
			return true;
	}
	return false;
}



int main () {
	using namespace std;
	using namespace std::chrono;

	vector<pair<string,int>> words;
	ifstream input = ifstream("tmp/WarAndPeace.txt");

	auto start = steady_clock::now();
	cout << "Parsing War and Peace" << endl;

	size_t nombre_lu = 0;
	size_t nombre_unique_lu = 0;
	// prochain mot lu
	string word;
	// une regex qui reconnait les caractères anormaux (négation des lettres)
	regex re( R"([^a-zA-Z])");
	while (input >> word) {
		// élimine la ponctuation et les caractères spéciaux
		word = regex_replace ( word, re, "");
		// passe en lowercase
		transform(word.begin(),word.end(),word.begin(),::tolower);


		if(!isInVec(words,word)){
			words.push_back(make_pair(word,1));
			nombre_unique_lu++;
		}
		else{
			for(auto& pair : words){
				if(pair.first == word)
					pair.second+=1;
			}
		}

		// word est maintenant "tout propre"
		if (nombre_lu % 100 == 0)
			// on affiche un mot "propre" sur 100
			cout << nombre_lu << ": "<< word << endl;
		nombre_lu++;
	}
	input.close();

	cout << "Finished Parsing War and Peace" << endl;

	auto end = steady_clock::now();
    cout << "Parsing took "
              << duration_cast<milliseconds>(end - start).count()
              << "ms.\n";

    cout << "Found a total of " << nombre_lu << " words." << endl;

    cout << "Found a total of " << nombre_unique_lu << " unique words." << endl;

    for(auto pair : words){
    	if(pair.first == "war" || pair.first == "peace")
    		cout << pair.first << " occurences : " << pair.second <<endl;
    }

    return 0;
}

