/*
 * String.h
 *
 *  Created on: 4 oct. 2019
 *      Author: 3602783
 */

#ifndef STRING_H_
#define STRING_H_
#include <ostream>

using namespace std;

namespace pr {

class String {
	char* str;

	public:
		String(char*);
		String(const String&);
		size_t length();
		size_t length(char*);
		char* newcopy(char*);
		~String();
		friend ostream & operator << (ostream & os, const String s);
		String& operator = (String& s);
		bool operator < (const String & b) const;
		friend bool operator==(const String &a,const String &b);
};

bool operator==(const String &a,const String &b);


}

#endif /* STRING_H_ */
