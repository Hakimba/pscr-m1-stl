/*
 * Strutil.h
 *
 *  Created on: 4 oct. 2019
 *      Author: 3602783
 */

#ifndef STRUTIL_H_
#define STRUTIL_H_

namespace pr {
int compare(char* s1, char* s2){
	while(*s1 && (*s1 == *s2)){
		s1++;
		s2++;
	}
	return *s1-*s2;
}
}



#endif /* STRUTIL_H_ */
