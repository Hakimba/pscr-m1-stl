/*
 * String.cpp
 *
 *  Created on: 4 oct. 2019
 *      Author: 3602783
 */




#include "String.h"
#include "Strutil.h"
#include <iostream>
namespace pr {


String::String(char* st){
	this->str = newcopy(st);
}

String::String(const String& s){
	this->str = newcopy(s.str);
}

size_t String::length(){
	int i = 0;
		while(this->str[i] != '\0'){
			i++;
		}
		return i;
}

size_t String::length(char* stl){
	int i = 0;
	while(stl[i] != '\0'){
		i++;
	}
	return i;
}

char * String::newcopy(char* stcp){
	int taille_str = length(stcp)+1;
	char* copy = new char[taille_str];
	for (int var = 0; var < taille_str; var++) {
		copy[var] = stcp[var];
	}

	return copy;
}

ostream & operator << (ostream & os, const String s){
	os << s.str;
	return os;
}

String& String::operator = (String& s){
	//if(this != &s){
		delete [] str;
	//}
	this->str = this->newcopy(s.str);
	return *this;
}

bool String::operator<(const String & b) const {
	return compare(this->str,b.str) < 1;
}

bool operator==(const String &a,const String &b){
	return compare(a.str,b.str) == 0;
}


String::~String(){
	delete [] str;
}

}
